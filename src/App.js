import React from 'react'
import InfiniteScroll from 'react-infinite-scroller'
import ProviderComponent, { Consumer } from './Context'
import Button from './Button'
import SearchBox from './Input/SearchBox'
import Dropdown from './Input/Dropdown'
import RadioGroup from './Input/RadioGroup'
import Card, { PlaceholderComponents } from './Card'
import style from './styles.css'

const mapCb = (review, index, groupName) => {
  if (Array.isArray(review)) {
    return <div key={`${review[0].created}${review[0].title}`}>
      {groupName[index]}
      {review.map(({ created, stars, reviewId, title, content, productTitle }) => (
        <Card
          key={`${reviewId}${stars}${reviewId}`}
          created={created}
          reviewId={reviewId}
          productTitle={productTitle}
          stars={stars}
          title={title}
          content={content}
        />
      ))}
    </div>
  } else {
    const { created, stars, reviewId, title, content, productTitle } = review

    return (
      <Card
        key={reviewId}
        created={created}
        reviewId={reviewId}
        productTitle={productTitle}
        stars={stars}
        title={title}
        content={content}
      />
    )
  }
}

const renderCards = (reviewsObj, filter, groupName) => {
  if (Object.keys(reviewsObj).length !== 0){
    if (filter === 0) {
      return reviewsObj.reviews.map((review, index) => mapCb(review, index, groupName))
    } else {
      if (Array.isArray(reviewsObj.reviews[0])) {
        return reviewsObj.reviews.map(reviewArr => (
          reviewArr.filter(({ stars }) => stars === filter)
          .map((review, index) => mapCb(review, index, groupName))
        ))
      } else {
        return reviewsObj.reviews
          .filter(({ stars }) => stars === filter)
          .map((review, index) => mapCb(review, index, groupName))
      }
    }
  } else {
    return <PlaceholderComponents />
  }
}

const App = () => {
  return (
    <ProviderComponent>
      <div className={style.container}>
        <div className={style.wrappedContainer}>
          <SearchBox />
          <Consumer>
            {({groupBy}) => (
              <Dropdown
                placeholder="Group by"
                options={[
                  {
                    value: 'groupByDay',
                    label: 'Group by day'
                  },
                  {
                    value: 'groupByWeek',
                    label: 'Group by week'
                  },
                  {
                    value: 'groupByMonth',
                    label: 'Group by month'
                  }
                ]}
                onChange={e => groupBy(e.target.value)}
              />
            )}
          </Consumer>
          <Consumer>
            {({ sortBy }) => (
              <Dropdown
                placeholder="Order by"
                options={[
                  {
                    value: 'latest',
                    label: 'Latest'
                  },
                  {
                    value: 'older',
                    label: 'Older'
                  }
                ]}
                onChange={e => sortBy(e.target.value)}
              />
            )}
          </Consumer>
          <RadioGroup />
        </div>
        <Button text="refresh"/>
        <Consumer>
          {({reviewsObj, filter, groupName, fetchNextPage}) => (
            <InfiniteScroll
              pageStart={0}
              loadMore={fetchNextPage}
              hasMore={reviewsObj.hasMore}
              loader={<div key={0}>Loading ...</div>}
            >
              {renderCards(reviewsObj, filter, groupName)}
            </InfiniteScroll>
          )}
        </Consumer>
      </div>
    </ProviderComponent>
  );
}

export default App;
