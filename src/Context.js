import React from 'react'
import moment from 'moment'

const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
]

export const { Provider, Consumer } = React.createContext()

export default class ProviderComponent extends React.Component {
  state = {
    reviewsObj: {},
    backUpState: {},
    filter: 0,
    groupName: [],
    nextPageToFetch: 2,
  }

  search = query => {
    if (query.length === 0) {
      this.setState({ reviewsObj: this.state.backUpState })
    } else {
      const filteredState = this.state.reviewsObj.reviews.filter(
        review => review.productTitle.indexOf(query) !== -1 || review.title.indexOf(query) !== -1 || review.content.indexOf(query) !== -1
      )

      this.setState({ reviewsObj: {
        reviews: filteredState,
        hasMore: this.state.reviewsObj.hasMore
      }})
    }
  }

  filterByStars = stars => this.setState({ filter: stars })

  sortingLatest = (a, b) => (
    a.created < b.created
      ? 1
      : -1
  )

  sortingOlder = (a, b) => (
    a.created > b.created
      ? 1
      : -1
  )

  sortBy = sort => {
    let sortedState = []

    if (sort === 'latest') {
      sortedState = this.state.reviewsObj.reviews.sort(this.sortingLatest)

      this.setState({
        reviewsObj: {
          reviews: sortedState,
          hasMore: this.state.reviewsObj.hasMore
        }
      })
    } else {
      sortedState = this.state.reviewsObj.reviews.sort(this.sortingOlder)

      this.setState({
        reviewsObj: {
          reviews: sortedState,
          hasMore: this.state.reviewsObj.hasMore
        }
      })
    }
  }

  validateAndPush = (validation, array, value) => {
    if (validation) {
      array[array.length - 1].push(value)
    } else {
      array.push([].concat(value))
    }
  }

  reducer = (validation) => {
    let groupName = []
    const reducedArr = this.state.reviewsObj.reviews.reduce((acc, currVal) => {
      if (acc.length === 0) {
        acc.push([].concat(currVal))
        return acc
      }

      if (acc.map(item => item.filter(i => i.reviewId === currVal.reviewId)).length === 0) {
        return acc
      } else {
        if (validation === 'groupByMonth'){
          this.validateAndPush(
            moment(acc[acc.length - 1][0].created).month() === moment(currVal.created).month(),
            acc,
            currVal
          )
        } else if (validation === 'groupByDay') {
          this.validateAndPush(
            moment(acc[acc.length - 1][0].created).month() === moment(currVal.created).month()
            && moment(acc[acc.length - 1][0].created).date() === moment(currVal.created).date(),
            acc,
            currVal
          )
        } else if (validation === 'groupByWeek') {
          this.validateAndPush(
            moment(acc[acc.length - 1][0].created).week() === moment(currVal.created).week(),
            acc,
            currVal
          )
        }
      }

      return acc
    }, [])

    if (validation === 'groupByMonth') {
      groupName = reducedArr.map(item => months[moment(item[0].created).month()])

      this.setState({
        reviewsObj: {
          reviews: reducedArr,
          hasMore: this.state.reviewsObj.hasMore
        },
        groupName
      })
    } else if (validation === 'groupByDay') {
      groupName = reducedArr
        .map(item => `${months[moment(item[0].created).month()]} ${moment(item[0].created).date()}`)
      this.setState({
        reviewsObj: {
          reviews: reducedArr,
          hasMore: this.state.reviewsObj.hasMore
        },
        groupName
      })
    } else if (validation === 'groupByWeek') {
      groupName = reducedArr
        .map(item => `Week number ${moment(item[0].created).week()} of the year`)
      this.setState({
        reviewsObj: {
          reviews: reducedArr,
          hasMore: this.state.reviewsObj.hasMore
        },
        groupName
      })
    }
  }

  groupBy = group => {
    if (group === 'groupByMonth') {
      this.reducer('groupByMonth')
    }else if (group === 'groupByDay') {
      this.reducer('groupByDay')
    } else if (group === 'groupByWeek') {
      this.reducer('groupByWeek')
    }
  }

  fetchNextPage = () => {
    fetch(`/${this.state.nextPageToFetch}`)
      .then(response => response.json())
      .then(data => {
        const concatData = {
          reviews: this.state.reviewsObj.reviews.concat(data.reviews),
          hasMore: data.hasMore
        }
        this.setState({ reviewsObj: concatData, nextPageToFetch: this.state.nextPageToFetch + 1 })
      })
  }

  fetchAndSetState = () => (
    fetch('/1')
      .then(response => response.json())
      .then(data => this.setState({ reviewsObj: data, backUpState: data }))
  )

  componentDidMount() {
    this.fetchAndSetState()
  }

  render() {
    const store = {
      reviewsObj: this.state.reviewsObj,
      search: this.search,
      filterByStars: this.filterByStars,
      filter: this.state.filter,
      sortBy: this.sortBy,
      groupBy: this.groupBy,
      groupName: this.state.groupName,
      fetchAndSetState: this.fetchAndSetState,
      fetchNextPage: this.fetchNextPage
    }
    return (
      <Provider value={store}>
        {this.props.children}
      </Provider>
    )
  }
}