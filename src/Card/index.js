import React from 'react'
import moment from 'moment'
import star from '../assets/star.svg'
import star2 from '../assets/hollowedStar.svg'
import style from './styles.css'

const renderStars = num => {
  let starsArr = []

  for(let i = 0; i < num; i++) {
    starsArr.push(star)
  }

  if (5 - num !== 0) {
    for (let i = 0; i < 5 - num; i++) {
      starsArr.push(star2)
    }
  }

  return starsArr
}

const Card = ({ created, stars, reviewId, title, content, productTitle }) => {
  return  (
    <div className={style.container}>
      <div className={style.details}>
        <div className={style.avatar}/>
        <div className={style.titleSeparator}>
          <h3 className={style.detailTitle}>date</h3>
          { moment(created).format('DD.MM.YYYY') }
        </div>
        <div className={style.titleSeparator}>
          <h3 className={style.detailTitle}>stars</h3>
          { renderStars(stars).map((el, index) => (<img className={style.star} key={index} src={el} alt="star"/>)) }
        </div>
        <div className={style.titleSeparator}>
          <h3 className={style.detailTitle}>{reviewId}</h3>
          {productTitle.slice(0, 14).concat('...')}
        </div>
      </div>
      <div>
        <h2>{ title }</h2>
        <p>{ content }</p>
      </div>
    </div>
  )
}

export const PlaceholderComponents = () => {
  return (
    <div className={`${style.container} ${style.containerPadding}`}>
      <div className={style.details}>
        <div className={style.avatar} />

        <div className={style.detailsPlaceholder} />

        <div className={style.detailsPlaceholder} />

        <div className={style.detailsPlaceholder} />
      </div>
      <div>
        <div className={style.textPlaceholder} />
        <div className={style.textPlaceholder} />
      </div>
    </div>
  )
}

export default Card