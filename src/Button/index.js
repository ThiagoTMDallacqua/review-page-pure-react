import React from 'react'
import { Consumer } from '../Context'
import styles from './styles.css'

const Button = ({ text }) => (
  <Consumer>
    {({ fetchAndSetState }) => (
      <button onClick={fetchAndSetState} className={styles.button}>{text}</button>
    )}
  </Consumer>
)

export default Button