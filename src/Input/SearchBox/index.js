import React from 'react'
import { Consumer } from '../../Context'
import style from './styles.css'
class SearchBox extends React.Component {
  render() {
    return (
      <Consumer>
        {({search}) => (
          <input
            className={style.searchBox}
            onChange={e => {
              search(e.target.value)
            }}
            type='text'
            placeholder='Search'
          />
        )}
      </Consumer>
    )
  }
}

export default SearchBox