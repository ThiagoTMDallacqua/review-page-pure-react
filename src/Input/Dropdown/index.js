import React from 'react'
import style from './styles.css'

const Dropdown = ({ options, onChange, placeholder }) => {
  return (
    <select className={style.select} onChange={onChange} defaultValue={'initial'}>
      <option value="initial" disabled>
        {placeholder}
      </option>
      {options.map(({ value, label }) => (
        <option key={label} value={value}>
          {label}
        </option>
      ))}
    </select>
  )
}

export default Dropdown