import React from 'react'
import { Consumer } from '../../Context'
import style from './styles.css'
import Star from '../../assets/grayStar.svg'

class RadioGroup extends React.Component {
  state = {
    options: [
      {
        label: '1',
        value: 1,
        clicked: false
      },
      {
        label: '2',
        value: 2,
        clicked: false
      },
      {
        label: '3',
        value: 3,
        clicked: false
      },
      {
        label: '4',
        value: 4,
        clicked: false
      },
      {
        label: '5',
        value: 5,
        clicked: false
      },
    ]
  }

  onClick = (options, option, cb) => {
    let noneChecked = true
    let newOptions = options
    for (let opt of newOptions) {
      if (opt.value !== option.value && !opt.clicked) continue
      
      opt.clicked = !option.clicked
      
      if (opt.clicked) noneChecked = false
      
      if (opt.value !== option.value && opt.clicked) opt.clicked = false
    }

    this.setState({ options: newOptions })

    if (noneChecked) {
      cb(0)
    } else {
      cb(option.value)
    }
  }

  render() {
    const { options } = this.state
    return (
      <div className={style.container}>
        FILTER BY:
      <div className={style.radioGroup}>
          {options.map(({ label, value, clicked }) => (
            <label key={label} className={style.label}>
              <Consumer>
                {({ filterByStars }) => (
                  <span
                    onClick={e => {
                      e.preventDefault()
                      this.onClick(options, { label, value, clicked }, filterByStars)
                    }}
                    className={style.spanBorder}
                  >
                    <span className={clicked ? style.clicked : ''} />
                  </span>
                )}
              </Consumer>
              <input key={label} className={style.radio} type="radio" value={value} />
              {label}
              <img src={Star} className={style.star} alt="star" />
            </label>
          ))}
        </div>
      </div>
    )
  }
}

export default RadioGroup